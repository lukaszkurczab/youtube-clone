import React, { useContext } from 'react'
import RecommendedVideos from './ViedeosSection'
import { SizeContext } from '../../context';

import './styles/style.css'

function Main() {
  const { defaultSize } = useContext(SizeContext);

  return (
    <main className={defaultSize ? "main bigMain" : "main"}>
      <div className="main__filmsWrapper">
        <RecommendedVideos />
      </div>
    </main>
  );
}

export default Main;
