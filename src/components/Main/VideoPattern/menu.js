import React from 'react'
import styled from 'styled-components'

const Button = ({ text, desc, icon, link }) => {
  return (
    <a href={link} className="main__listItem buttonWrapper">
      <img src={icon} alt={desc} className="main__listImg" />
      <p className="main__listText">{text}</p>
    </a>
  )
}

function Menu(props) {
  const posX = window.innerWidth - props.posX > 282 ? 'left: 0' : 'right: 0'
  const posY = window.innerHeight - props.posY > 272 ? '' : 'top: -272px'

  const Wrapper = styled.div`
  ${posX};
  ${posY};
  position: absolute;
  z-index: 100;
  `

  return (
    <Wrapper>
      <div className="main__listWrapper">
        <div className="main__list">
          <Button text="Dodaj do kolekcji" desc="Add to queue" icon="./images/queue.svg" link="#"/>
          <Button text="Zapisz na liście Do obejrzenia" desc="Save" icon="./images/time.svg" link="#"/>
          <Button text="Zapisz na playliście" desc="Save on playlist" icon="./images/saveOnList.svg" link="#"/>
        </div>
        <div className="main__list">
          <Button text="Nie interesuje mnie to" desc="I'm not interested" icon="./images/notInterested.svg" link="#"/>
          <Button text="Nie polecaj mi tego kanału" desc="Don't recommend me this" icon="./images/dontRecommend.svg" link="#"/>
          <Button text="Zgłoś" desc="Report" icon="./images/flag.svg" link="#"/>
        </div>
      </div>
    </Wrapper>

  )
}

export default Menu