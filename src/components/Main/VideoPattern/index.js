import React, { useState } from 'react'
import Menu from './menu'

import './styles/style.css'

function Video({ title, channel, id }) {
  const [active, setActive] = useState(false)
  const [posX, setPosX] = useState(0)
  const [posY, setPosY] = useState(0)

  window.addEventListener('click', (e) => {
    if (e.target.parentNode.id !== `videoButton${id}`) {
      setActive(false)
    }
  })

  return (
    <div id={"video" + id} className="main__filmWrapper">
      <div className="main__filmShortWrapper">
        <img src="./images/videoPlaceholder.png" alt="" className="main__filmShort" />
        <div className="main__videoButtonWrapper">
          <div className="main__videoButtonIconWrapper">
            <img src="./images/clock.svg" alt="" className="main__videoButtonIcon" />
          </div>
          <div className="main__videoTextWrapper">
            <p className="main__videoText">Do obejrzenia</p>
          </div>
        </div>
        <div className="main__videoButtonWrapper">
          <div className="main__videoButtonIconWrapper">
            <img src="./images/queue.svg" alt="" className="main__videoButtonIcon" />
          </div>
          <div className="main__videoTextWrapper">
            <p className="main__videoText">Dodaj do kolejnki</p>
          </div>
        </div>
        <div className="main__filmTimeWrapper">
          <p className="main_filmTime">14:58</p>
        </div>
      </div>
      <div className={active ? "main__descWrapper active" : "main__descWrapper"}>
        <div className="main__authorAvatarWrapper">
          <img src="./images/profil1.jpg" alt="" className="main__authorAvatar profilAvatar" />
        </div>
        <div className="main__textWrapper">
          <div className="main__titleWrapper">
            <h2 className="main__title">
              {title}
            </h2>
          </div>
          <div className="main__channelWrapper">
            <p className="main__text">{channel}</p>
          </div>
          <p className="main__text main__views">50 tys. wyświetleń</p>
          <p className="main__text">2 lata temu</p>
        </div>
        <div className="main__optionsWrapper">
          <button aria-label="show more options" id={"videoButton" + id} className="main__more" onClick={(e) => { setActive(!active); setPosX(e.clientX); setPosY(e.clientY) }}>
            <img src="./images/dots.svg" alt="" className="main__optionsImg" />
          </button>
          {active && <Menu posX={posX} posY={posY} />}
        </div>
      </div>
    </div>
  );
}

export default Video;
