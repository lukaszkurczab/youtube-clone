import React, { useState } from 'react'
import Menu from './menu'

import './styles/style.css'

const Button = ({ text, desc, icon }) => {
  return (
    <button className="labelToggler">
      <img src={icon} alt={desc} className="main__listImg" />
      <div className="label">
        <p>{text}</p>
        </div>
    </button>
  )
}

function Post({ id }) {
  const [active, setActive] = useState(false)
  const [posX, setPosX] = useState(0)
  const [posY, setPosY] = useState(0)

  window.addEventListener('click', (e) => {
    if (e.target.parentNode.id !== `postButton${id}`) {
      setActive(false)
    }
  })

  return (
    <div id={"post" + id} className={active ? "innerSection__postWrapper active" : "innerSection__postWrapper"}>
      <div className="innerSection__postHeader">
        <img src="./images/profil1.jpg" alt="" className="innerSection__channelLogo profilAvatar" />
        <p className="main__text">Lorem ipsum</p>
        <p className="main__text">21 minut temu</p>
      </div>
      <div className="innerSection__postMain">
        <div className="innerSection__postMainText">Lorem ipsum dolor sit amet consectetur adipisicing elit. Distinctio, quia exercitationem dolor, odit itaque accusamus quae quidem voluptates obcaecati necessitatibus expedita totam! Rem sint autem asperiores quisquam molestiae ratione inventore.</div>
        <img src="./images/videoPlaceholder.png" alt="" className="innerSection__postMainImg" />
      </div>
      <div className="innerSection__postFooter">
        <div className="innerSection__votesWrapper">
          <Button text="Podoba mi się" desc="Like" icon="./images/like.svg" />
          <span className="innerSection__voteCount innerSection__text">256</span>
          <Button text="Nie podoba mi się" desc="Dislike" icon="./images/dislike.svg" />
        </div>
        <div className="innerSection__commentsWrapper">
        <Button text="Skomentuj" desc="Comments" icon="./images/comment.svg" />
          <span className="innerSection__commentNumber innerSection__text">23</span>
          <div className="main__buttonWrapper">
            <button aria-label="show more options" id={"postButton" + id} className="main__more" onClick={(e) => { setActive(!active); setPosX(e.clientX); setPosY(e.clientY) }}>
              <img src="./images/dots.svg" alt="" />
            </button>
            {active && <Menu posX={posX} posY={posY} />}
          </div>
        </div>
      </div>
    </div>
  )
}

export default Post