import React from 'react'
import Button from '../../Button'

import './style.css'

function AsideBig() {
  return (
    <aside className="aside">
      <aside className="bigAside ownScrollBar">
        <div className="listSection">
          <Button text="Główna" desc="Main page" icon="./images/home.svg" link="#" additionalClasses="active" />
          <Button text="Na czasie" desc="Hot" icon="./images/fire.svg" link="#" />
          <Button text="Subskrybcje" desc="Subscriptions" icon="./images/subscriptions.svg" link="#" />
        </div>
        <div className="listSection">
          <Button text="Biblioteka" desc="Library" icon="./images/library.svg" link="#" />
          <Button text="Historia" desc="History" icon="./images/history.svg" link="#" />
          <Button text="Twoje filmy" desc="Yours films" icon="./images/play.svg" link="#" />
          <Button text="Do obejrzenia" desc="To watch" icon="./images/clock.svg" link="#" />
          <Button text="Filmy, które mi się podobają" desc="Liked videos" icon="./images/like.svg" link="#" />
        </div>
        <div className="listSection">
          <h2 className="aside__title">Subskrybcje</h2>
          <Button text="Lorem ipsum" desc="Profile" icon="./images/profil1.jpg" link="#" additionalClasses="color profilAvatar" />
          <Button text="Lorem ipsum" desc="Profile" icon="./images/profil2.jpg" link="#" additionalClasses="color profilAvatar" />
          <Button text="Lorem ipsum" desc="Profile" icon="./images/profil3.jpg" link="#" additionalClasses="color profilAvatar" />
        </div>
        <div className="listSection">
          <h2 className="aside__title">WIĘCEJ Z YOUTUBE</h2>
          <Button text="YouTube Premium" desc="YouTube Premium" icon="./images/youtubeOnlyIcon.svg" link="#" />
          <Button text="Filmy" desc="Films" icon="./images/film.svg" link="#" />
          <Button text="Gry" desc="Games" icon="./images/game.svg" link="#" />
          <Button text="Na żywo" desc="Live" icon="./images/live.svg" link="#" />
        </div>
        <div className="listSection">
          <Button text="Ustawienia" desc="Settings" icon="./images/cog.svg" link="#" />
          <Button text="Historia raportów" desc="Raport history" icon="./images/flag.svg" link="#" />
          <Button text="Pomoc" desc="Help" icon="./images/help.svg" link="#" />
          <Button text="Prześlij opinię" desc="Send opinion" icon="./images/error.svg" link="#" />
        </div>
        <div className="listSection">
          <div className="aside__linksWrapper">
            <button className="aside__link">Informacje</button>
            <button className="aside__link">Centrum prasowe</button>
            <button className="aside__link">Prawa autorskie</button>
            <button className="aside__link">Skontaktuj się z nami</button>
            <button className="aside__link">Twórcy</button>
            <button className="aside__link">Reklamy</button>
            <button className="aside__link">Deweloperzy</button>
          </div>
          <div className="aside__linksWrapper">
            <button className="aside__link">Warunki</button>
            <button className="aside__link">Prywatności</button>
            <button className="aside__link">Zasady i bezpieczeństwo</button>
            <button className="aside__link">Jak działa YouTube</button>
            <button className="aside__link">Przetestuj nowe funkcje</button>
          </div>
          <p className="licence">© 2021 Google LLC</p>
        </div>
      </aside>
    </aside>
  );
}

export default AsideBig;

