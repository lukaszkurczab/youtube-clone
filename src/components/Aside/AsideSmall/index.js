import React from 'react'
import Button from '../../Button'

import './style.css'

function AsideSmall() {
  return (
    <>
      <aside className="aside smallAside">
        <div className="aside__section">
        <Button text="Główna" desc="Main page" icon="./images/home.svg" link="#" additionalClasses="active" />
          <Button text="Na czasie" desc="Hot" icon="./images/fire.svg" link="#" />
          <Button text="Subskrybcje" desc="Subscriptions" icon="./images/subscriptions.svg" link="#" />
          <Button text="Biblioteka" desc="Library" icon="./images/library.svg" link="#" />
        </div>
      </aside>
    </>
  );
}

export default AsideSmall;

