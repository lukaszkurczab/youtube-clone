import React from 'react'
import Button from '../../Button'

function Menu() {

  return (
    <div className="header__listWrapper">
      <div className="listSection">
        <Button text="Prześlij film" desc="Send video" icon="./images/colorNew.png" link="#" additionalClasses="color" />
        <Button text="Transmituj na żywo" desc="Start streaming" icon="./images/colorLive.png" link="#" additionalClasses="color" />
      </div>
    </div>
  )
}

export default Menu