import React, { useState } from 'react'
import Menu from './menu'
import Button from '../Button'

import './style.css'

function Notifications() {
  const [showNotificationsMenu, toggleShowNotificationsMenu] = useState(false)

  window.addEventListener('click', (e) => {
    if (e.target.id !== "notificationsButtonToggler" &&
      e.target.id !== "notificationsButtonTogglerWrapper") {
      toggleShowNotificationsMenu(false)
    }
  })

  return (
    <div className="header__iconWrapper button notificationsButtonToggler" onClick={() => toggleShowNotificationsMenu(!showNotificationsMenu)}>
      <Button text="Powiadomienia" icon="./images/notification.svg" desc="Notifications" id="notificationsButtonToggler" />
      {showNotificationsMenu ? <Menu /> : null}
    </div>
  )
}

export default Notifications