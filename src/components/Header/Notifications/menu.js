import React from 'react'
import Notification from './notificationPattern'

function Menu() {

  return (
    <div className="notificationsBox header__listWrapper">
      <div className="header__listTitleWrapper">
        <h2 className="header__listTitle">Powiadomienia</h2>
        <div className="notifications__imgWrapper">
          <img src="./images/settings.svg" className="notifications__Img" alt="settings" />
        </div>
      </div>
      <ul id="notificationsWrapper" className="header__list ownScrollBar">
        <Notification />
      </ul>
    </div>
  )
}

export default Menu