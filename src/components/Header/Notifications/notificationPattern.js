import React from 'react'

function Notification() {
  return (
    <>
      <li>
        <button className="notificationWrapper">
          <div className="notification__avatarWrapper">
            <img src="./images/profil1.jpg" alt="" className="notification__avatar profilAvatar" />
          </div>
          <div className="notification__textWrapper">
            <h3 className="notification__text">Na kanał Lorem Ipsum został przesłany film Lorem ipsum dolor</h3>
            <span className="notification__time">1 dzień temu</span>
          </div>
          <div className="notification__filmShortWrapper">
            <img src="./images/videoPlaceholder.png" alt="" className="main__filmShort" />
          </div>
        </button>
      </li>
    </>
  )
}

export default Notification