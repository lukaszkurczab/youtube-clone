import React from 'react'
import Notifications from './Notifications'
import Logo from './Logo'
import Search from './Search'
import Create from './Create'
import Applications from './Applications'
import UserPanel from './UserPanel'

import './styles/style.css'

function Main() {
  return (
    <header className="header">
      <Logo />
      <Search />
      <div className="header__userOptionsWrapper">
        <Create />
        <Applications />
        <Notifications />
        <UserPanel />
      </div>
    </header>
  );
}

export default Main;
