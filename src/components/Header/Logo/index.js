import React, { useContext } from 'react'
import { SizeContext } from '../../../context';

import './style.css'

function Logo() {
  const { setSize } = useContext(SizeContext)


  return (
    <div className="logo__wrapper">
      <button aria-label="side navigation toggle" className="logo__sideNavToggler button" onClick={setSize}>
        <img src="./images/bars.svg" alt="toggle side navigation" />
      </button>

      <div className="logo__homeButtonWrapper">
        <button aria-label="home page button" className="logo__homeButtonImg">
          <img src="./images/youtube.svg" alt="go to home page" />
        </button>
        <span className="header__localization">PL</span>
      </div>
    </div>
  )
}

export default Logo