import React, { useState } from 'react'
import Button from '../Button'

import './style.css'

function Search() {
  const [largeSearch, toggleLargeSearch] = useState(false)

  window.addEventListener('resize', () => {
    if (window.innerWidth > 660) {
      toggleLargeSearch(false)
    } else {
      toggleLargeSearch(true)
    }
  })

  window.onload = () => {
    if (window.innerWidth < 660) {
      toggleLargeSearch(true)
    }
  }

  return (
    <div className={largeSearch ? "searchWrapper active" : "searchWrapper"}>
      <div className="backWrapper" onClick={() => toggleLargeSearch(false)}>
        <img src="./images/arrowBack.svg" alt="go back" />
      </div>
      <form action="#" className="search__seachBoxWrapper">
        <div className="searchAreaWrapper">
          <input type="text" className="searchInput" placeholder="Szukaj" />
          <button><img src="./images/keyboard.png" alt="turn on screen keyboar" className="searchKeyboard" /></button>
        </div>

        <button className="search__submitWrapper labelToggler">
          <input type="submit" value="" className="search__submitImg" />
          <div className="label">
            <p>Szukaj</p>
          </div>
        </button>
      </form>

      <Button text="Wyszukaj głosowo" desc="Turn on microphone" icon="./images/microphone.svg" />

      {largeSearch && <Button text="Szukaj" desc="Show search area" icon="./images/search.svg" onClick={() => toggleLargeSearch(true)} />}
    </div>
  )
}

export default Search