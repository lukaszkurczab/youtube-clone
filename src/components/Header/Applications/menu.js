import React from 'react'
import Button from '../../Button'

function Menu() {

  return (
    <div className="header__listWrapper">
      <div className="listSection">
        <Button text="YouTube TV" desc="YouTube TV" icon="./images/colorYouTubeTV.svg" link="#" additionalClasses="color" />
        <Button text="YouTube Music" desc="YouTube Music" icon="./images/colorYouTubeMusic.svg" link="#" additionalClasses="color" />
        <Button text="YouTube Kids" desc="YouTube Kids" icon="./images/colorYouTubeKids.svg" link="#" additionalClasses="color" />
      </div>
      <div className="listSection">
        <Button text="Akademia twórców" desc="Creators academy" icon="./images/colorYouTube.svg" link="#" additionalClasses="color" />
        <Button text="YouTube dla wykonawców" desc="For artists" icon="./images/colorYouTube.svg" link="#" additionalClasses="color" />
      </div>
    </div>
  )
}

export default Menu