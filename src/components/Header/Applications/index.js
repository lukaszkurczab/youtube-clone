import React, { useState } from 'react'
import Menu from './menu'
import Button from '../Button'

function Applications() {
  const [showMenu, toggleShowMenu] = useState(false)

  window.addEventListener('click', (e) => {
    if (e.target.id !== "aplicationsToggler" &&
      e.target.id !== "aplicationsTogglerWrapper") {
      toggleShowMenu(false)
    }
  })

  return (
    <div className="header__iconWrapper button aplicationsToggler" onClick={() => toggleShowMenu(!showMenu)}>
      <Button text="Aplikacje YouTube" icon="./images/options.svg" desc="More aplications" id="aplicationsToggler" />
      {showMenu && <Menu />}
    </div>
  )
}

export default Applications