import React from 'react'
import Button from '../../Button'

function Menu() {

  return (
    <div className="header__listWrapper userPanel ownScrollBar">
      <div className="listSection">
        <div className="header__listItemWrapper">
          <div className="header__accountWrapper">
            <div className="header__accountImgWrapper">
              <img src="./images/avatar.jpg" alt="" className="header__accountImg profilAvatar" />
            </div>
            <div className="header__accountTextWrapper">
              <p className="header__accountName">Lorem Ipsum</p>
              <p className="header__accountMail">loremipsum@example.com</p>
              <button className="header__accountManage">Zarządzaj kontem Google</button>
            </div>
          </div>
        </div>
      </div>
      <div className="listSection">
        <Button text="Twój kanał" desc="Yours channel" icon="./images/channel.svg" link="#" />
        <Button text="Zakupy i subskrypcje" desc="Shop and subscriptions" icon="./images/cash.svg" link="#" />
        <Button text="YouTube Studio" desc="YouTube Studio" icon="./images/youTubeCog.svg" link="#" />
        <Button text="Przełącz konto" desc="Change account" icon="./images/channelSwap.svg" link="#" />
        <Button text="Wyloguj się" desc="Log out" icon="./images/logout.svg" link="#" />
      </div>
      <div className="listSection">
        <Button text="Wygląd: tryb urządzenia" desc="Mode" icon="./images/mode.svg" link="#" />
        <Button text="Język: polski" desc="Language" icon="./images/language.svg" link="#" />
        <Button text="Lokalizacja: Polska" desc="Localization" icon="./images/localization.svg" link="#" />
        <Button text="Ustawienia" desc="Settings" icon="./images/cog.svg" link="#" />
        <Button text="Twoje dane w YouTube" desc="Your data on YouTube" icon="./images/security.svg" link="#" />
        <Button text="Pomoc" desc="Help" icon="./images/help.svg" link="#" />
        <Button text="Prześlij opinię" desc="Send opinion" icon="./images/error.svg" link="#" />
        <Button text="Skróty klawiszowe" desc="Keyboard shortcuts" icon="./images/keyboard.svg" link="#" />
      </div>
    </div>
  )
}

export default Menu