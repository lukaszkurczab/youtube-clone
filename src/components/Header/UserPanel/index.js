import React, { useState } from 'react'
import Menu from './menu'

function UserPanel() {
  const [userPanel, toggleUserPanel] = useState(false)

  window.addEventListener('click', (e) => {
    if (!e.target.classList.contains("header__profilePictureWrapper") &&
      !e.target.classList.contains("header__profilePicture")) {
      toggleUserPanel(false)
    }
  })

  return (
    <div className="header__profilePictureWrapper" onClick={() => toggleUserPanel(!userPanel)}>
      <img src="./images/avatar.jpg" className="header__profilePicture" alt="profile" />
      {userPanel ? <Menu /> : null}
    </div>
  )
}

export default UserPanel