import { useState } from 'react';
import React from 'react';
import Main from '../components/Main';
import Header from '../components/Header';
import Aside from '../components/Aside';

import { SizeContext, SiteSize } from '../context';
import './styles/style.css'

function Homepage() {
  const [defaultSize, setDefaultSize] = useState(SiteSize.defaultSize);

  const setSize = () => setDefaultSize(prevValue => !prevValue);

  let windowWidth = window.innerWidth

  window.addEventListener('resize', () => {
    let newWidth = window.innerWidth

    if (newWidth < 1000 && windowWidth >= 1000) {
      setDefaultSize(true);
      windowWidth = newWidth
    } else if (newWidth >= 1000 && windowWidth < 1000) {
      setDefaultSize(false);
      windowWidth = newWidth
    }
  })

  window.onload = () => {
    if (windowWidth < 1000) {
      setDefaultSize(true);
    }
  }

  return (
    <>
      <SizeContext.Provider value={{ defaultSize, setSize }}>
        <Header />
        <Aside />
        <Main />
      </SizeContext.Provider>
    </>
  )
}

export default Homepage